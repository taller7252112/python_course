"""
MAKE A GAME

CONDITION
a. 8 attempts to complete
b. If the user hasn't guessed, say they didn't make it

INPUT
a. The program asks for a name from the user
b. The program requests a number between 1 and 100

OUTPUT
a. Number less than 1 and greater than 100
b. Number less than the PROGRAM'S
c. Number less than the PROGRAM'S
d. Correct number and attempts

# INPUT
name = input('What\'s your name? ')
user_number = input('Choose a number between 1 and 100')

"""
from random import *

# OTHER
print("Hey let's play a game, try to guess a number between 1 to 100. You got 8 attempts :)")
game_number = randint(0,100)

# INPUT
name = input('What\'s your name? ')
user_number = input('Choose a number between 1 and 100 ' )
user_number = int(user_number)

# LOGICS AND MESSAGES FOR OUTPUT

counter = 0

while counter <= 8:
    if (user_number < 1) and (user_number>100):
        print(f"{name}, the chosen number {user_number} is out of range (i.e., less than 1 and greater than 100!).")
    elif (user_number < game_number):
        print(f"{name},  the chosen number {user_number} is less than the number to guess!")
    elif (user_number > game_number):
        print(f"{name},  the chosen number {user_number} is greater than the number to guess!")
    elif (user_number == game_number):
        print(f"{name},  the chosen number {user_number} is the number to guess. CONGRATS!" + '\n End of the game"')
        break

    user_number = input('\nChoose a number between 1 and 100 ')
    user_number = int(user_number)
    counter +=1

    if counter > 8:
        print(f'\n Hey {name} the number of attempts is over. See you next time!')