from pathlib import *
import os

# Ruta
def obtener_ruta():
    ruta = Path("C:/Users/ebarrero/PycharmProjects/pythonProject/Día 6/Recetas")
    return ruta

# Contador de archivos en ruta
def contar_recetas():
    ruta_base = obtener_ruta()
    contador_recetas= 0
    for carpeta_actual, _, archivos in os.walk(ruta_base): #Contador de archivos
        contador_recetas += len(archivos)
    return contador_recetas

def mensaje_usuario():
    contador_recetas_var = contar_recetas()
    ruta_var = obtener_ruta()
    print("Hola, bienvenid@ a tu recetario")
    print(f"Tus recetas están ubicadas en {ruta_var} y en total tienes {contador_recetas_var} recetas")


def menu():
    mensaje_usuario()
    eleccion = 0
    while eleccion != 4 :
        print('\nQué deseas hacer: \n1. Leer recetas \n2. Crear receta \n3. Eliminar receta \n4. Salir')
        eleccion = int(input())
        if eleccion ==1:
            leer_recetas()
        elif eleccion == 2:
            crear_receta()
        elif eleccion == 3:
            eliminar_receta()



def leer_recetas():
    while True:

        print('\nSeleccionaste la opción 1. Leer recetas, escoge entre las siguientes categorías: ')

        directorio_categorias = 'C:/Users/ebarrero/PycharmProjects/pythonProject/Día 6/Recetas'

        contenido_directorio = os.listdir(directorio_categorias)

        categorias = [item for item in contenido_directorio if os.path.isdir(os.path.join(directorio_categorias, item))]

        # Imprimir categorías disponibles
        if not categorias:
            print('No hay categorías disponibles')
            continue  # Reiniciar el bucle para preguntar de nuevo
        else:
            for indice, categoria in enumerate(categorias):
                print(indice + 1, categoria)
# A DONDE VA EL ANTIRIOR
            eleccion_categoria = input('\n Elige una categoría por su número de índice: ')

            try:
                eleccion_categoria = int(eleccion_categoria)
                if 1 <= eleccion_categoria <= len(categorias):
                    categoria_seleccionada = categorias[eleccion_categoria - 1]
                    print(f'\nHas elegido la categoría: {categoria_seleccionada}')

                    # Obtener la ruta completa de la categoría seleccionada
                    ruta_categoria = os.path.join(directorio_categorias, categoria_seleccionada)

                    # Listar los archivos disponibles en la categoría seleccionada
                    archivos_categoria = os.listdir(ruta_categoria)

                    if not archivos_categoria:
                        print(f'La categoría {categoria_seleccionada} no contiene archivos disponibles.')
                    else:
                        print(f'\nArchivos disponibles en la categoría {categoria_seleccionada}:')
                        for indice, archivo in enumerate(archivos_categoria):
                            print(indice + 1, archivo)

                        eleccion_archivo = input('\nElige un archivo por su número de índice para ver su contenido: ')
                        try:
                            eleccion_archivo = int(eleccion_archivo)
                            if 1 <= eleccion_archivo <= len(archivos_categoria):
                                archivo_seleccionado = archivos_categoria[eleccion_archivo - 1]
                                ruta_archivo = os.path.join(ruta_categoria, archivo_seleccionado)

                                # Leer y mostrar el contenido del archivo
                                with open(ruta_archivo, 'r', encoding='utf-8') as archivo:
                                    contenido = archivo.read()
                                    print(f'\nContenido del archivo {archivo_seleccionado}:\n')
                                    print(contenido)
                                break  # Salir del bucle cuando se ha mostrado el contenido
                            else:
                                print('Elección incorrecta. Debe ser un número válido de índice de archivo.')
                        except ValueError:
                            print('Debes ingresar un número de índice válido para el archivo.')

                    # trabajar con los archivos dentro de la categoría seleccionada
                else:
                    print('Elección incorrecta. Debe ser un número válido de índice de categoría.')
            except ValueError:
                print('Debes ingresar un número de índice válido para la categoría.')


def crear_receta():
    print('\nSeleccionaste la opción 2. Crear recetas, escoge dentro de cuál categoría añadirás la receta: ')
    directorio_categorias = 'C:/Users/ebarrero/PycharmProjects/pythonProject/Día 6/Recetas'

    contenido_directorio = os.listdir(directorio_categorias)

    categorias = [item for item in contenido_directorio if os.path.isdir(os.path.join(directorio_categorias, item))]

    if not categorias:
        print('No hay categorías disponibles')

    else:
        for indice, categoria in enumerate(categorias):
            print(indice + 1, categoria)

        eleccion_categoria = input('\nElige una categoría por su número de índice: ')

        try:
            eleccion_categoria = int(eleccion_categoria)
            if 1 <= eleccion_categoria <= len(categorias):
                categoria_seleccionada = categorias[eleccion_categoria - 1]
                print(f'\nHas elegido la categoría: {categoria_seleccionada}')

                nombre_receta = input('Ingresa el nombre de la nueva receta: ')
                ruta_receta = os.path.join(directorio_categorias, categoria_seleccionada, nombre_receta)

                if not os.path.exists(ruta_receta):
                    os.makedirs(ruta_receta)
                    print(f'Se ha creado la receta "{nombre_receta}" en la categoría "{categoria_seleccionada}"')
                else:
                    print(f'La receta "{nombre_receta}" ya existe en la categoría "{categoria_seleccionada}"')

            else:
                print('Selección de categoría no válida.')
        except ValueError:
            print('Debes ingresar un número válido.')


# Llamada a la función para crear una receta


def crear_receta():
    print('\nSeleccionaste la opción 2. Crear recetas, escoge dentro de cuál categoría añadirás la receta: ')
    directorio_categorias = 'C:/Users/ebarrero/PycharmProjects/pythonProject/Día 6/Recetas'

    contenido_directorio = os.listdir(directorio_categorias)

    categorias = [item for item in contenido_directorio if os.path.isdir(os.path.join(directorio_categorias, item))]

    if not categorias:
        print('No hay categorías disponibles')
    else:
        for indice, categoria in enumerate(categorias):
            print(indice + 1, categoria)

        eleccion_categoria = input('\nElige una categoría por su número de índice: ')

        try:
            eleccion_categoria = int(eleccion_categoria)
            if 1 <= eleccion_categoria <= len(categorias):
                categoria_seleccionada = categorias[eleccion_categoria - 1]
                print(f'\nHas elegido la categoría: {categoria_seleccionada}')

                nombre_receta = input('Ingresa el nombre de la nueva receta: ')
                ruta_receta = os.path.join(directorio_categorias, categoria_seleccionada, f'{nombre_receta}.txt')

                if not os.path.exists(ruta_receta):
                    with open(ruta_receta, 'w') as archivo_receta:
                        archivo_receta.write('Contenido de la receta...')
                    print(f'Se ha creado la receta "{nombre_receta}" en la categoría "{categoria_seleccionada}"')
                else:
                    print(f'La receta "{nombre_receta}" ya existe en la categoría "{categoria_seleccionada}"')

            else:
                print('Selección de categoría no válida.')
        except ValueError:
            print('Debes ingresar un número válido.')


def eliminar_receta():
    print('\nSeleccionaste la opción 3. Eliminar recetas, escoge de cuál categoría eliminarás la receta: ')
    directorio_categorias = 'C:/Users/ebarrero/PycharmProjects/pythonProject/Día 6/Recetas'

    contenido_directorio = os.listdir(directorio_categorias)

    categorias = [item for item in contenido_directorio if os.path.isdir(os.path.join(directorio_categorias, item))]

    if not categorias:
        print('No hay categorías disponibles')
        return  # Sale de la función si no hay categorías disponibles

    for indice, categoria in enumerate(categorias):
        print(indice + 1, categoria)

    eleccion_categoria = input('\nElige una categoría por su número de índice: ')

    try:
        eleccion_categoria = int(eleccion_categoria)
        if 1 <= eleccion_categoria <= len(categorias):
            categoria_seleccionada = categorias[eleccion_categoria - 1]
            print(f'\nHas elegido la categoría: {categoria_seleccionada}')

            # Listar recetas en la categoría seleccionada
            ruta_categoria = os.path.join(directorio_categorias, categoria_seleccionada)
            recetas = os.listdir(ruta_categoria)

            if not recetas:
                print(f'No hay recetas disponibles en la categoría "{categoria_seleccionada}"')
            else:
                print('Recetas disponibles en esta categoría:')
                for indice, receta in enumerate(recetas):
                    print(indice + 1, receta)

                eleccion_receta = input('\nElige una receta por su número de índice para eliminar: ')

                try:
                    eleccion_receta = int(eleccion_receta)
                    if 1 <= eleccion_receta <= len(recetas):
                        receta_a_eliminar = recetas[eleccion_receta - 1]
                        ruta_receta = os.path.join(ruta_categoria, receta_a_eliminar)

                        # Confirmación de eliminación
                        confirmacion = input(
                            f'\n¿Estás seguro de eliminar la receta "{receta_a_eliminar}"? Ingresa "1" para borrar o "0" para cancelar: ').strip()
                        if confirmacion == '1':
                            os.remove(ruta_receta)
                            print(f'Se ha eliminado la receta "{receta_a_eliminar}"')
                        elif confirmacion == '0':
                            print('Eliminación cancelada.')
                        else:
                            print('Entrada no válida. Eliminación cancelada.')
                    else:
                        print('Selección de receta no válida.')
                except ValueError:
                    print('Debes ingresar un número válido para la receta.')
        else:
            print('Selección de categoría no válida.')
    except ValueError:
        print('Debes ingresar un número válido para la categoría.')



# Llamada a la función para eliminar una receta


menu()