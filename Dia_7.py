class Persona():
    def __init__(self, nombre, apellido):
        self.nombre = nombre
        self.apellido = apellido

class Cliente(Persona):
    def __init__(self, numero_cuenta, balance, nombre, apellido):
        super().__init__(nombre,apellido)
        self.numero_cuenta = numero_cuenta
        self.balance = balance

pepito = Cliente(1234, 200, 'Elma', 'Rano')
def imprimir_datos():

    return f" {pepito.nombre} {pepito.apellido}\nTu número de cuenta es: {pepito.numero_cuenta}\nTu balance es: {pepito.balance}"


def depositar():
    deposito = input('Ingresa la cantidad que vas a depositar ')
    pepito.balance = pepito.balance + int(deposito)
    return f'Su balance al depositar {deposito} es {pepito.balance}'

def retirar():
    retiro = input('Indica la cantidad del retiro ')
    retiro= int(retiro)
    if retiro>pepito.balance:
        return 'Fondos insuficientes'
    else:
        pepito.balance = pepito.balance - int(retiro)
        return f' Su balance al retirar {retiro} es {pepito.balance}'


def menu():
    datos = imprimir_datos()
    print(f'Hola, {datos}')
    while True:
        print(f'\nQué deseas hacer hoy: \n1. Depositar \n2. Retirar \n3. Salir \n')
        opcion = int(input())

        if opcion == 1:
            print(depositar() + '\n')
        elif opcion == 2:
            print(retirar() + '\n')
        elif opcion == 3:
            print('Hasta pronto y no vuelvas nunca <3')
            break  # Sale del bucle si el usuario elige la opción 3
        else:
            print('Opción no válida, elige 1, 2 o 3. \n')


menu()
