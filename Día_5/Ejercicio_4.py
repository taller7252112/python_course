"""
Escribe una función llamada contar_primos() que requiera un
solo argumento numérico.
Esta función va a mostrar en pantalla todos los números
primos existentes en el rango que va desde cero hasta ese
número incluido, y va a devolver la cantidad de números
primos que encontró

# Función definir si es primo
def contar_primos(numero):
    lista = []
    if numero <= 1:
        return False
    for elemento in range(2,(numero)):
        if numero % elemento == 0:
            return False
    return True

"""
# Función contar primo
def contar_primos(numero):
    cantidad_primos = 0
    for num in range(2, numero + 1):
        if num <= 1:
            continue
        es_primo = True
        for divisor in range(2, int(num ** 0.5) + 1):
            if num % divisor == 0:
                es_primo = False
                break
        if es_primo:
            cantidad_primos += 1
    return cantidad_primos

print(contar_primos(52))






