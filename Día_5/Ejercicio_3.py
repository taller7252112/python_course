"""
Escribe una función que requiera una cantidad indefinida de
argumentos. Lo que hará esta función es devolver True si en
algún momento se ha ingresado al numero cero repetido dos
veces consecutivas.
"""
def ceros_repetidos (*args):
    num_previo = None
    for numero in args:
        if numero == 0 and num_previo == 0:
            return True
        num_previo = numero
    return False

print(ceros_repetidos(1,2,3,0,0,5))