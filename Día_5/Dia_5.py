"""
Juego del ahorcado
Escoger una palabra secreta y le va a mostrar
los guiones que representan el númerode de letras de
una palabra.

Si el usuario acierta, colocar las letras
que son parte de la palabra sobre los guiónes que le ocupan

Si el usuario no acierta, pierde una vida de un total de 6

Tips:
1. Importar método choice para elegir palabras al azar en una lista creada por mí
2. Funciones para: pedir letra, validar letra,, chequear letra está, ver si ganó
3. Dibujar diagrama de flujo
"""
import random
# Listado de palabras
def listado_palabras():
    lista = ["Algebra", "Geometria", "Calculo", "Estadística", "Trigonometria", "Derivada", "Integral", "Teorema", "Fraccion", "Ecuacion",
    # Categoría 2: Libros de la Biblia
    "Genesis", "Exodo", "Levitico", "Numeros", "Deuteronomio", "Salmos", "Isaias", "Mateo", "Lucas", "Apocalipsis",
    # Categoría 3: Súper Héroes
    "Superman", "Batman", "SpiderMan", "WonderWoman", "IronMan", "Hulk", "Thor", "BlackWidow", "CapitáaAmérica", "Aquaman",
    # Categoría 4: Países de Europa
    "Francia", "Alemania", "Italia", "España", "Inglaterra", "Portugal", "Suecia", "Polonia", "Holanda", "Suiza",
    # Categoría 5: Capitales de Sudamérica
    "Bogota", "BuenosAires", "Lima", "Brasilia", "Santiago", "Caracas", "Quito", "Asunción", "Montevideo", "LaPaz"]

    lista_convertida = [palabra.lower() for palabra in lista]
    return lista_convertida
    # print(lista_convertida)
    # Comprobación conversión a minúscula

# Escoger unan palabra de la lista
def escoger_palabra():
    lista = listado_palabras()
    palabra_ahorcado = random.choice(lista)
    return palabra_ahorcado

# Mensaje usuario y escoger palabra
def pedido_usuario():
    print("Hola, vamos a jugar al ahorcado.")
    nombre = input("¿Cuál es tu nombre? ")
    print(f"{nombre}, tendrás 6 vidas para adivinar la palabra")
    return nombre # Para cuando se haya completado el juego usar el nombre en el llamado de la función

def logica_juego():
    inicio = pedido_usuario()
    listado_letra = []
    palabra = escoger_palabra()
    palabra_enmascarada = "-" * len(palabra)
    print(palabra_enmascarada)
    desaciertos = 0  # Contador de desaciertos

    while True:
        letra = input("Inserta una letra: ")

        if letra.isalpha() and letra.islower() and len(letra) == 1:  # validar condición letra
            acierto = False  # Variable para verificar si hubo un acierto en esta iteración

            for i, elemento in enumerate(palabra):  # Recorrer cada letra en la palabra con su índice
                if letra == elemento:
                    listado_letra.append(letra)  # Agregar letra al listado de aciertos
                    acierto = True
                    print("Acierto")
                    # Actualizar la palabra enmascarada
                    palabra_enmascarada = actualizar_palabra_enmascarada(palabra, palabra_enmascarada, letra)
                    print(palabra_enmascarada)
                    break  # Salir del bucle for si hay un acierto

            if not acierto:
                print("Desacierto")
                desaciertos += 1  # Incrementar el contador de desaciertos
                if desaciertos >= 6:
                    print("Has acumulado 6 desaciertos. ¡Juego terminado!")
                    break  # Terminar el juego si se acumulan 6 desaciertos

            if set(listado_letra) == set(palabra):
                print(f"{inicio} ¡Has adivinado la palabra!")
                break  # Salir del bucle si se adivina la palabra completa

        else:
            print("Por favor, ingresa una única letra minúscula válida.")

    listado_letra = palabra
    return listado_letra  # Retorna listado de letras acertadas en el mismo orden que la palabra

# Función para actualizar la palabra enmascarada con las letras acertadas
def actualizar_palabra_enmascarada(palabra_original, palabra_enmascarada, letra):
    nueva_palabra_enmascarada = ""
    for i, elemento in enumerate(palabra_original):
        if letra == elemento:
            nueva_palabra_enmascarada += letra
        else:
            nueva_palabra_enmascarada += palabra_enmascarada[i]
    return nueva_palabra_enmascarada

print(logica_juego())



