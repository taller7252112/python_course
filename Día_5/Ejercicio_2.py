"""
Escribe una función (puedes ponerle cualquier nombre que
quieras) que reciba cualquier palabra como parámetro, y que
devuelva todas sus letras únicas (sin repetir) pero en orden
alfabético.
"""
def cualquier_nombre (palabra):
    palabra = palabra.lower()
    palabra_ordenada = sorted(set(palabra))
    return palabra_ordenada

print(cualquier_nombre("Caramelito"))
