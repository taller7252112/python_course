"""
Crea una función llamada devolver_distintos() que reciba 3
integers como parámetros.
Si la suma de los 3 numeros es mayor a 15, va a devolver el
número mayor.
Si la suma de los 3 numeros es menor a 10, va a devolver el
número menor.
Si la suma de los 3 números es un valor entre 10 y 15
(incluidos) va a devolver el número de valorintermedio.

"""
from random import randint
def devolver_distinto(int1, int2, int3):
    suma = int1 + int2 + int3
    maxi = max(int1, int2, int3)
    mini = min(int1, int2, int3)
    medi = suma - maxi - mini
    if suma > 15:
        # condicion mayor de los tres
        return maxi

    elif suma < 10:
        # condicion menor de los tres
        return mini
    else:
        # condicion intermedio de los tres
        return medi

a = randint(0,15)
b = randint(0,15)
c = randint(0,15)
llamado = (devolver_distinto(a,b,c))
print(f"a = {a}, b = {b}, c =  {c}. Escogido {llamado}")