class Dia_8_numerador:
    def __init__(self):
        self.turno_generador = self.generador_turnos()

    def decorador_turnos(self, funcion):
        def otra_funcion():
            turno = next(self.turno_generador)
            print('Buen día, su turno es:', turno)
            print('\nPronto será atendido en', funcion.__name__)
            funcion()

        return otra_funcion

    def generador_turnos(self):
        valor = 1
        while True:
            yield valor
            valor += 1

