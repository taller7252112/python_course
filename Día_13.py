import pyttsx3
import speech_recognition as sr
import pywhatkit
import yfinance as yf
import pyjokes
import webbrowser
import datetime
import wikipedia
import pyaudio


# Escuchar micrófono y devolver audio como texto
def transformar_audio_en_texto():
    # Almacenar recognizer en una variable
    r = sr.Recognizer()  # Agrega los paréntesis para instanciar Recognizer

    # Configurar el micrófono
    with sr.Microphone() as origen:

        # Tiempo de espera
        r.pause_threshold = 0.8  # Corrige la propiedad pause_threshold

        # Informar comienzo de grabación
        print('Ya puedes hablar')

        # Guardar lo que escuche como audio
        audio = r.listen(origen)

        try:
            # buscar en Google lo que escuche
            pedido = r.recognize_google(audio, language="es-col")

            # Prueba de que puedo ingresar
            print('Dijiste: ' + pedido)

            # Devolver pedido
            return pedido

        # En caso de que no comprenda
        except sr.UnknownValueError:
            # Prueba de que no entendió
            print("Ups, no entendí")

            # Devolver error
            return 'Sigo esperando'

        # En caso de no resolver el pedido
        except sr.RequestError:

            # Prueba de que no comprendió el audio
            print('Ups, no entendí')

            # Devolver error
            return 'Sigo esperando'

        # Error inesperado
        except:

            # Prueba de que no comprendió el audio
            print('Ups, algo salió mal')

            # Devolver error
            return 'Sigo esperando'

# funcion para que pueda ser escuchado el asistente
def hablar(mensaje):
    # Bases de voces
    id1 = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_ZIRA_11.0'
    id2 = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_DAVID_11.0'
    id3 = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_ES-MX_SABINA_11.0'

    # Inicializar el motor pyttsx3
    engine = pyttsx3.init()
    engine.setProperty('voice', id3)
    # Pronunciar mensaje
    engine.say(mensaje)
    engine.runAndWait()

#Informar el día de la semana
def pedir_dia():
    # Crear variable con datos de hoy
    dia = datetime.date.today()
    print(dia)

    # Crear variable para el día de semana
    dia_semana = dia.weekday()
    print(dia_semana)

    # diccionario con nombres de días
    calendario = {0: 'Lunes',
                  1: 'Martes',
                  2: 'Miércoles',
                  3:   'Jueves',
                  4: 'Viernes',
                  5: 'Sábado',
                  6: 'Domingo'}

    # Decir el día de la semana
    hablar(f'Hoy es {calendario[dia_semana]}')

# Pedir hora actual
def pedir_hora():
    # crear una variable con datos de la hora
    hora = datetime.datetime.now()
    hora = f'En este momentos son las{hora.hour} horas con {hora.minute} minutos'
    print(hora)

    # decir hora
    hablar(hora)

# Funcion Saludo inicial
def saludo_inicial():

    # Crear variable con datos de hora
    hora = datetime.datetime.now()
    if hora.hour >= 0 and hora.hour < 12:
        momento = 'Buen día'

    elif hora.hour >=12 and hora.hour <18:
        momento = 'Buenas tardes'

    else:
        momento = 'Buenas noches'


    # decir el saludo
    hablar(f' {momento}. Soy Miriam, ¿Cómo te puedo ayudar?')

# Centros de pedidos
def pedir_cosas():

    # Activar saludo inicial
    saludo_inicial()

    # Variable de corte
    comenzar = True

    # loop central
    while comenzar:
        # Activar micrófono y guardar pedido en string
        pedido = transformar_audio_en_texto().lower()

        if 'abrir youtube' in pedido:
            hablar('Espero que sea para estudiar. Abriendo Youtube')
            webbrowser.open('https://www.youtube.com/')
            continue

        elif 'abrir el navegador' in pedido:
            hablar('Bueno, pero nada de cochinadas')
            webbrowser.open('https://www.google.com/')
            continue

        elif 'qué día es hoy' in pedido:
            pedir_dia()
            continue

        elif 'qué hora es' in pedido:
            pedir_hora()
            continue

        elif 'buscar en wikipedia' in pedido:
            hablar('Así me gusta, estudiando. Buscando eso en Wikipedia')
            pedido = pedido.replace('buscar en wikipedia', '')
            wikipedia.set_lang('es')
            resultado = wikipedia.summary(pedido, sentences=1)
            hablar('Wikipedia dice lo siguiente: ')
            hablar(resultado)
            continue

        elif 'buscar en internet' in pedido:
            hablar('Vale, ya mismo pues.')
            pedido = pedido.replace('buscar en internet', '')
            pywhatkit.search(pedido)
            hablar('Esto es lo que he encontrado')
            continue

        elif 'reproducir' in pedido:
            hablar('Bueno, pero sin hacer mucho ruido')
            pedido = pedido.replace('reproducir', '')
            pywhatkit.playonyt(pedido)
            continue

        elif 'chiste' in pedido:
            hablar(pyjokes.get_joke('es'))
            continue

        elif 'precio de las acciones' in pedido:
            accion = pedido.split('de')[-1].strip()
            cartera = {'apple': 'APPL',
                       'amazon': 'AMZN',
                       'google': 'GOOGL'}
            try:
                accion_buscada = cartera[accion]
                accion_buscada = yf.Ticker(accion_buscada)
                precio_actual = accion_buscada.info['regularMarketPrice']
                hablar(f'La encontré, el precio de {accion} es {precio_actual}')
                continue
            except:
                hablar('Perdón, pero no la he encontrado')
                continue

        elif 'cerrar' in pedido:
            hablar('Vale, que te vaya muy bien, y juicio!')
            break

        else:
            hablar('Mucha demora, y hay mucho qué hacer.')

pedir_cosas()