"""
PROYECTO DEL DÍA:
1. Crear un turnero para una farmacia
2. Tres áreas para turnero: Perfumería, Farmacia y Cosmeticos
3. Preguntar a cuál área se dirige y dar el turno
4. Sistema debe llevar cuenta y usar generaddores
5. decir su turno es y al final aguarde y será atendido. DECORADORES
6. Dos módulos numerador: generadores y decorador y el otro principal: funciones

"""

from Dia_8_numerador import Dia_8_numerador

perfumeria_numerador = Dia_8_numerador()
farmacia_numerador = Dia_8_numerador()
cosmetiqueria_numerador = Dia_8_numerador()

# Aplica el decorador a cada función con su respectiva instancia
@perfumeria_numerador.decorador_turnos
def perfumeria():
    pass

@farmacia_numerador.decorador_turnos
def farmacia():
    pass

@cosmetiqueria_numerador.decorador_turnos
def cosmetiqueria():
    pass

def eleccion():
    while True:
        try:
            print('\nA donde se dirige: \n1. Perfumería \n2. Farmacia \n3. Cosmetiquería \n')
            eleccion = int(input())

            if eleccion == 1:
                perfumeria()

            elif eleccion == 2:
                farmacia()

            elif eleccion == 3:
                cosmetiqueria()

            elif eleccion == 825:
                print('Programa terminado')
                break
            else:
                print('\nValor inválido')

        except ValueError:
            print("\nElemento no válido, escoja una opción de nuevo")

eleccion()
