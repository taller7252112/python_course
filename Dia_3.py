# Proyecto del dia

# Entradas
texto = input("Ingrese un texto de libre elección: ")

letra1 = input("Ingrese la primer letra de su elección: ")
letra2 = input("Ingrese la segunda letra de su elección: ")
letra3 = input("Ingrese la tercer letra de su elección: ")

# Conteo de letras
texto = texto.lower()
encontrar_letra_1 = texto.count(letra1)
encontrar_letra_2 = texto.count(letra2)
encontrar_letra_3 = texto.count(letra3)

# Conteo de palabras
palabras = texto.split()
total_palabras = len(palabras)

# Primera y última letra
lista = list(texto)

# Orden invertido
palabras_invertidas = list(palabras)
palabras_invertidas = ' '.join(palabras_invertidas[::-1])

# Está Python?
python = "python" in texto
diccionario = {True:"Sí", False:"No"}

# Muestra Conteo de letras
print(f"\n1. La {letra1} aparece {encontrar_letra_1} veces. La {letra2} aparece {encontrar_letra_2} veces."
      f" La {letra3} aparece {encontrar_letra_3} veces. ")

# Muestra de palabras
print(f"2. El total de palabras en el texto es {total_palabras}")

# Muestra Primera y última letra del texto
print(f"3. El primer elemento del texto es {lista[0]} y el último elemento del texto es {lista[-1]}")

# Muestra el orden invertido de las palabras
print(f"4. el texto al reves sería: \n\t\t\t\t\t\t{palabras_invertidas}")

# Muestra Python
print(f"La palabra Python {diccionario[python]} aparece:")
