import bs4
import requests

"""
Code designed by Fede Garay from Escuela Directa
Web Scraping Example

"""
# crear url sin número de página
url_base = "https://books.toscrape.com/catalogue/page-{}.html"

# lista de títulos 4 o 5 estrellas
titulos_rating_alto = []

# iterar páginas
for pagina in range(1,51):

    # Crear sopa en cada página
    url_pagina = url_base.format(pagina)
    resultado = requests.get(url_pagina)
    sopa = bs4.BeautifulSoup(resultado.text, 'lxml')

    #Seleccionar datos de los libros
    libros = sopa.select('.product_pod')

    # iterar libros
    for libro in libros:
        # chequear estrellas
        if len(libro.select('.star-rating.Four')) != 0 or len(libro.select('.star-rating.Five')):
            # guardar título en variable
            titulo_libro = libro.select('a')[1]['title']

            # agregar libro a la lista
            titulos_rating_alto.append(titulo_libro)
# ver libros de 4 o 5 estrellas:
for t in titulos_rating_alto:
    print(t)
