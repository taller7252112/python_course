"""
Notas de clase
Pedir dato  y asignarlo
nombre = input("dime tu nombre")
print("Tu nombre es" + nombre)

a = "Hola "
b = "Neas "
frase = a + b
print(a+b)

mi_numero = 1 + 10.3
print(type(mi_numero))
print(mi_numero + mi_numero)

num1 = 5.8
print(num1)
print(type(num1))

num2 = int(num1)  # casting
print(num2)
print(type(num2))

edad = input("Dime tu edad: ")
edad = int(edad)
nueva_edad = 1 + edad
print("tu edad será: " + str(nueva_edad))

x = 10
y = 5

# formatear cadenas tipo 1
print("Mis números son {} y {}".format(x,y))

# formatear cadenas tipo 2
color = "rojo"
matricula = 541926
print(f"El auto es {color} y su matrícula {matricula}")

x = 6
y = 2
z = 7
print(f"{x} mas {y} es igual a {x+y}")
print(f"{x} menos {y} es igual a {x-y}")
print(f"{x} por {y} es igual a {x*y}")
print(f"{x} dividido {y} es igual a {x/y}")
print(f"{z} divido al piso de {y} es igual a {z//y}")
print(f"{z} modulo de {y} es igual a {z%y}")
print(f"{x} elevado a la 2 {y} es igual a {x**y}")
print(f"La raiz cuadrado de {x} es {x**0.5}")

resultado = round(90/7)
print(resultado)

valor = round(95.666666, 2)
print(valor)
"""
# Proyecto del día

nombre = input("Cuál es su nombre? ")
ventas = input("Cuánto ha vendido durante el mes? ")
ventas = float(ventas)
comisiones = round(ventas*0.13, 2)
print(f'{nombre}, por sus ventas de {ventas}, usted recibirá una comisión de {comisiones}')
