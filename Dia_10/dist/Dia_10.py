"""
Game Designed by Fede Garay from Escuela Directa
Program was created as part of a project for the course Python Total


"""

import math
import random
import pygame
import io
# Inicializar Pygame
pygame.init()

# Declarar pantalla
pantalla = pygame.display.set_mode((800, 600))
se_ejecuta = True

# Titulo e Ícono
pygame.display.set_caption("Space in")
icono = pygame.image.load('outer-space-alien.png')
pygame.display.set_icon(icono)
fondo = pygame.image.load('background.png')

# Variable Jugador
img_jugador = pygame.image.load('rocket.png') # 64 pix
img_jugador = pygame.transform.scale(img_jugador, (60, 60))

jugador_x = 352
jugador_y = 535
jugador_x_cambio = 0

# Variable Rival
img_rival = []
rival_x = []
rival_y = []
rival_x_cambio = []
rival_y_cambio = []
cantidad_enemigos = 5  # Ajusta la cantidad de enemigos según tus necesidades

for e in range(cantidad_enemigos):
    img_rival.append(pygame.image.load('ufo.png'))  # Agregar la imagen a la lista
    rival_x.append(random.randint(0, 736))
    rival_y.append(random.randint(50, 200))
    rival_x_cambio.append(3)
    rival_y_cambio.append(50)

# Variable bala
img_bala = pygame.image.load('laser.png') # 64 pix

bala_x = 0
bala_y = 500
bala_x_cambio = 0
bala_y_cambio = 3
bala_visible = False

# Puntaje
puntaje = 0


def fuente_bytes(param):
    pass


fuente_como_bytes = fuente_bytes("Freesansbold.ttf")
fuente = pygame.font.Font(fuente_como_bytes, 32)
texto_x = 10
texto_y = 10


# Funcion fuentes
def fuente_bytes(fuente):
    with open(fuente, 'rb') as f:
        ttf_bytes = f.read()
    return io.BytesIO(ttf_bytes)

# Función Mostrar Puntaje
def mostrar_puntaje(x,y):
    texto = fuente.render(f'Puntaje: {puntaje}', True, (10,10,10))
    pantalla.blit(texto, (x,y))
# Funcion jugador
def jugador(x,y):
    pantalla.blit(img_jugador, (x, y))

# Funcion rival
def rival(x,y, ene):
    pantalla.blit(img_rival[ene], (x, y))

# Funcion disparar bala
def disparar_bala(x,y):
    global bala_visible
    bala_visible = True
    pantalla.blit(img_bala, (x + 16,y + 10))

# función detectar colisiones
def hay_colision(x_1,y_1,x_2,y_2):
    distancia = math.sqrt(math.pow(x_2-x_1,2) + math.pow(y_2-y_1,2))
    if distancia < 27:
        return True
    else:
        return False

# Loop para cerrar pantalla
while se_ejecuta:
    # Color pantalla RGB
    pantalla.blit(fondo, (0,0))

    # Jugabilidad presionar flechas
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            se_ejecuta = False

        # Lógica del movimiento
        if evento.type == pygame.KEYDOWN:
            if evento.key == pygame.K_LEFT:
                jugador_x_cambio = -2.5
            if evento.key == pygame.K_RIGHT:
                jugador_x_cambio = 2.5
            if evento.key == pygame.K_SPACE:
                if not bala_visible:
                    bala_x = jugador_x
                    disparar_bala(bala_x, bala_y)

        # Jugabilidad soltar flechas
        if evento.type == pygame.KEYUP:
            if evento.key == pygame.K_LEFT or evento.key == pygame.K_RIGHT:
                jugador_x_cambio = 0

    # Modificar ubicación jugador
    jugador_x += jugador_x_cambio

    # Mantener en bordes jugador
    if jugador_x <= 0:
        jugador_x = 0
    elif jugador_x >= 735:
        jugador_x = 735

    # Modificar ubicación Rival
    for e in range(cantidad_enemigos):
        rival_x[e] += rival_x_cambio[e]

        # Mantener en bordes Rival Horizontal
        if rival_x[e] <= 0:
            rival_x_cambio[e] = 3
            rival_y[e] += rival_y_cambio[e]
        elif rival_x[e] >= 735:
            rival_x_cambio[e] = -3
            rival_y[e] += rival_y_cambio[e]

        # Movimiento vertical del Rival
        if rival_y[e] <= 300:
            rival_y[e] += rival_y_cambio[e]
        else:
            rival_y_cambio[e] = 3  # Cambia la dirección para volver a subir

        # Colision
        colision = hay_colision(rival_x[e], rival_y[e], bala_x, bala_y)
        if colision:
            bala_y = 500
            bala_visible = False
            puntaje += 1
            rival_x[e] = random.randint(0, 736)
            rival_y[e] = random.randint(50, 200)

        rival(rival_x[e], rival_y[e], e)

        if puntaje >= 15:
            se_ejecuta = False

    # Movimiento Bala
    if bala_y <= -64:
        bala_y = 500
        bala_visible = False
    if bala_visible:
        disparar_bala(bala_x, bala_y)
        bala_y -= bala_y_cambio

    jugador(jugador_x, jugador_y)

    mostrar_puntaje(texto_x, texto_y)
    # Actualizar
    pygame.display.update()
